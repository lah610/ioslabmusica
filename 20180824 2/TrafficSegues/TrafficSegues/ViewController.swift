//
//  ViewController.swift
//  TrafficSegues
//
//  Created by Barragán Edgar on 24/08/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    @IBOutlet weak var Switch1: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToRed(unwindSegue : UIStoryboardSegue){
        
    }

    @IBAction func Green(_ sender: Any) {
        if Switch1.isOn{
            performSegue(withIdentifier: "NO", sender: nil)
        }
    }
    
    @IBAction func Yellow(_ sender: Any) {
        if Switch1.isOn{
            performSegue(withIdentifier: "YES", sender: nil)
        }
    }
}

