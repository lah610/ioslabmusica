//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let userInputAge = "5t4"
let userAge = Int(userInputAge)

if let unwrappedNumber = userAge {
    print(unwrappedNumber)
}
else {
    print("Por favor ingrese un numero valido")
}


////

struct Toddler {
    var name : String
    var monthsOld : Int
    
    init?(name : String, monthsOld : Int){ ///Inicializador de la estructura, inicializador fallido
        if(monthsOld > 12){
            return nil
        }
        else {
            self.name = name
            self.monthsOld = monthsOld
        }
    }
}


let myboy = Toddler(name: "Andy", monthsOld: 34)
if let unwrappedToddle = myboy {
    print(unwrappedToddle.monthsOld)
}
else{
    print("No se puede instanciar la estructura")
}

/////

/*class Person {
    var age : Int
    var resident : Resident?
}

class Resident {
    var address : Address?
}

class Address {
    var buildingNumber : String?
    var streetName : String?
    var apartmentNumber: String?
}*/


struct Person {
    var age : Int
    var resident : Resident?
}

struct Resident {
    var address : Address?
}

struct Address {
    var buildingNumber : String?
    var streetName : String?
    var apartmentNumber: String?
}


let person  = Person(age: 10, resident: Resident(address: Address(buildingNumber: "10", streetName: "La calle de la wea", apartmentNumber: "32")))
if let theResidence = person.resident {
    if let theAddress = theResidence.address {
        if let theApartmentNumber = theAddress.apartmentNumber{
            print("He/She lives in apartment number\(theApartmentNumber).")
        }
        else {
            print("No tiene numero de departamento")
        }
    }
    else {
        print("No tiene direccion")
    }
}
else {
    print("No tiene residencia")
}

if let theApartmentNumberA = person.resident?.address?.apartmentNumber{
    print("He/She lives in apartment number\(theApartmentNumberA).")
}


////
var items: [Any] = [5,"Bill",5.6,false]





