//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


class Alumno: CustomStringConvertible{
    
    var nombre : String = "nobody"
    var description : String {
        return "Yo soy \(nombre)"
    }
}

var luis = Alumno()

print(luis)


struct Empleado: Equatable, Comparable, CustomStringConvertible, Codable{
    var nombre: String
    var apellido : String
    var edad : Int
    
    static func ==(lhs: Empleado, rhs: Empleado) -> Bool{ //Equatable
        return lhs.nombre == rhs.nombre && lhs.apellido == rhs.apellido
    }
    
    static func < (lhs: Empleado, rhs: Empleado) -> Bool{ //Comparable
        return lhs.edad < rhs.edad
    }
    
    var description: String{ // CustomStringConvertible
        return "\(nombre) - \(edad)"
    }
}

var godin1 = Empleado(nombre: "Pedro", apellido: "Mola", edad : 19)

var godin2 = Empleado(nombre: "Omar", apellido: "Wea", edad : 25)
var godin3 = Empleado(nombre: "Pedro", apellido: "Mola", edad : 19)
var godin4 = Empleado(nombre: "Omar", apellido: "Wea", edad : 21)
var godin5 = Empleado(nombre: "Pedro", apellido: "Mola", edad : 17)

var empleados = [godin1, godin2, godin3, godin4, godin5]


let empleadosOrdenados =  empleados.sorted(by: <)
for empleados in empleadosOrdenados{
    print(empleados)
}

if godin1 == godin3{
    print("Son iguales")
}else{
    print("Son diferentes")
}


let jsonEncoder = JSONEncoder()
if let jsonData = try? jsonEncoder.encode(godin1), let jsonString = String(data: jsonData, encoding: .utf8
    ){
    print(jsonString)
}

protocol NombreCompleto {
    var nombreCompleto : String{get}
    func diNombreCompleto()
}

struct Persona : NombreCompleto {
    var nombre :String
    var apellido : String
    
    var nombreCompleto: String{
        return "\(nombre) \(apellido)"
    }
    
    func diNombreCompleto() {
        print(nombreCompleto)
    }
}

var edgar = Persona(nombre: "Edgar", apellido: "Barragan")
edgar.diNombreCompleto()

