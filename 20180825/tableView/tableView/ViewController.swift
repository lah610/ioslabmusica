//
//  ViewController.swift
//  tableView
//
//  Created by Barragán Edgar on 25/08/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var nombres = ["Brian","Omar","Kong","Edgar","Brian","Omar","Kong","Edgar","Brian","Omar","Kong","Edgar","Brian","Omar","Kong","Edgar"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nombres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "celdaExample"
        let cell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = nombres[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        let cell =  tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        
        let alertController  = UIAlertController(title: "Alumnos", message: "Hola Prros", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        //let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: <#T##((UIAlertAction) -> Void)?##((UIAlertAction) -> Void)?##(UIAlertAction) -> Void#>)////<#T##((UIAlertAction) -> Void)?##((UIAlertAction) -> Void)?##(UIAlertAction) -> Void#> Accion que se creara al dar clic
        
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }


}

