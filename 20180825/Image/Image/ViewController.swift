//
//  ViewController.swift
//  Image
//
//  Created by Barragán Edgar on 25/08/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import UIKit
//UIImagePickerControllerDelegate accede a la galeria
//UINavigationControllerDelegate pbtine foto de la galeria a la app
class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var photo: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func takePhoto(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Camara disponible")
        }else {
            print("No disponible la camara")
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print("Foto seleccionada")
        
        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            photo.image = selectedImage
            dismiss(animated: true, completion: nil)
        }
    }
    
}

