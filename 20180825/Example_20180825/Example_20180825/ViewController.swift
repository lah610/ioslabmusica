//
//  ViewController.swift
//  Example_20180825
//
//  Created by Barragán Edgar on 25/08/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("ViewController - View Did Load")
        
    }
    
    override func viewWillAppear(_ animated: Bool) { //Agregue esta funcione como ejemplo
        super.viewWillAppear(animated)
        
        print("ViewController - View Will Appear")
        
    }
    //Agregue esta funcione como ejemplo 
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("ViewController - View Did Appear")
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

