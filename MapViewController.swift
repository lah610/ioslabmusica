//
//  MapViewController.swift
//  FiveSenses
//
//  Created by Ricardo Yepez on 10/19/18.
//  Copyright © 2018 fi.unam.mx. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate{

    @IBOutlet weak var mqpView: MKMapView!
    
    @IBAction func didChangeStyle(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.mqpView.mapType = .standard
        case 1:
            self.mqpView.mapType = .satellite
        case 2:
            self.mqpView.mapType = .hybrid
            
        default:
            break
        }
        
    }
    let manager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        manager.requestWhenInUseAuthorization()
        mqpView.showsUserLocation = true
        mqpView.delegate = self
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        enableBasicLocationServices()

        // Do any additional setup after loading the view.
    }
    func enableBasicLocationServices() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            manager.requestWhenInUseAuthorization()
            break
            
        case .restricted, .denied:
            // Disable location features
            disableMyLocationBasedFeatures()
            break
            
        case .authorizedWhenInUse, .authorizedAlways:
            // Enable location features
            enableMyWhenInUseFeatures()
            break
        }
    }
    
    func enableMyWhenInUseFeatures(){
        manager.startUpdatingLocation()
    }
    
    func disableMyLocationBasedFeatures(){
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return}
        print(location.coordinate)
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            disableMyLocationBasedFeatures()
            break
            
        case .authorizedWhenInUse:
            enableMyWhenInUseFeatures()
            break
            
        case .notDetermined, .authorizedAlways:
            break
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
