//
//  Sesision.swift
//  LabTunes
//
//  Created by LUIS ARENAS HERNANDEZ on 09/11/18.
//  Copyright © 2018 LUIS ARENAS HERNANDEZ. All rights reserved.
//

import Foundation

class Sesision : NSObject
{
    var  token:  String? //regresa cuando hay un login exitoso, un solo usuario,una sola sesion
    static let sharedInstance = Sesision()//no necesita instancia almacena el "singleton"
    //un vez que se instancia por ser cte, no se modifica
    //con el patron de single aseguramos que exista una sola instancia
    override private init()//no puede llamar al init, desde otra clase,
    {
        super.init()
    }
    
    func saveSession ()
    {
        token = "yoMero"
    }
}
