//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by LUIS ARENAS HERNANDEZ on 09/11/18.
//  Copyright © 2018 LUIS ARENAS HERNANDEZ. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase
{

    override func setUp()
    {
        // Put setup code here. This method is called before the invocation of each test method in the class.cuando la prueba empieza
        let session = Sesision.sharedInstance
        session.token = nil
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class. prueba finaliza
    }
    
    func testCorrecLogin ()//cada vez que se cree laa funcion con test aparece un rombo
    {
        XCTAssertTrue(User.login(userName: "iOSLab", password: "Test"))
        
    }
    
    func testWrongLogin()
    {
        XCTAssertFalse(User.login(userName: "Raul", password:""))
    }
   
    func testSaveSession()
    {
        let sesion = Sesision.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "223")
        XCTAssertNotNil(sesion.token)
    }
    
    func testFailSaveSession()
    {
        let session = Sesision.sharedInstance
        let _ User.login(userName: "Jorge", password: "223")
    XCTAssertEqual(session.token)
    
    }
    
    func testExpectedToken()
    {
        let _  = User.login(userName: "iOSLab", password: "223")
        let session = Sesision.sharedInstance
        XCTAssertEqual(session.token!, "1234567890", "Token Should Match")
    }
    
    func testMusicSongs()
    {
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Downloaded")
        Music.fecthSongs
            {
               (songs) in
                resultSongs = songs
                promise.fulfill()
            }
        waitForExpectations(timeout: 5, handler:nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
}
//xcode-select --install
