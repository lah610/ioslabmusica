//
//  ActionButton.swift
//  WaterCounter
//
//  Created by Ricardo Yepez on 10/13/18.
//  Copyright © 2018 Ricardo Yepez. All rights reserved.
//

import UIKit

@IBDesignable
class ActionButton: UIButton {
    
    @IBInspectable var isAddButton: Bool = true
    
    @IBInspectable var fillColor: UIColor = UIColor.green

    
    private var halfWidth: CGFloat{
            return bounds.width / 2
    }
    
    private var halfHeight: CGFloat{
        return bounds.height / 2
    }
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        //Pintar el circulo
        let path = UIBezierPath(ovalIn: rect)
        fillColor.setFill()
        path.fill()
        //un circulo del tamaño del original * 0.6
        let plusWidth: CGFloat = min(bounds.width, bounds.height) * 0.6
        //la mitad de ese tamaño
        let halfPlusWidth = plusWidth / 2
        
        //Curva de Bezier tamaño 5
        let plusPath = UIBezierPath()
        plusPath.lineWidth = 5.0
        //
        plusPath.move(to: CGPoint(x: halfWidth-halfPlusWidth, y: halfWidth))
        
        plusPath.addLine(to: CGPoint(x: halfWidth+halfPlusWidth, y: halfHeight))
        
        if isAddButton {
            plusPath.move(to: CGPoint(x: halfWidth ,y: halfHeight - halfPlusWidth))
            plusPath.addLine(to: CGPoint(x: halfWidth, y: halfHeight +  halfPlusWidth))
        }
        
        
        
        UIColor.white.setStroke()
        
        plusPath.stroke()
        
        
    }
 

}
