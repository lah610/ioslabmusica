//
//  FishService.swift
//  plasticfishes-mvc
//
//  Created by Luis Ezcurdia on 9/14/18.
//  Copyright © 2018 Luis Ezcurdia. All rights reserved.
//

import Foundation

struct FishService {
    static let shared = FishService()
    
    let client = Client(baseURLComponentes: URLComponents(string: "https://plasticfishes.herokuapp.com)!)
    
    let jsonDecoder = JSONDecoder()
    
    static func list_all() -> [Fish] {
        let decoder = JSONDecoder()
        debugPrint(DataSource().fishes)
        let result = try? decoder.decode([Fish].self, from: DataSource().fishes)
        return result ?? []
    }
    func All (_ completion: ([Fish])->Void)
    {
        client.get(path: "/api/fishes)
    }
    
}
