//
//  client.swift
//  plasticfishes-mvc
//
//  Created by Barragán Edgar on 22/09/18.
//  Copyright © 2018 Luis Ezcurdia. All rights reserved.
//

import Foundation

class Client {
    //let baseURL = URL(string: "http://plasticfishes.herokuapp.com:3000/")
    static let shared = Client()
    
    static let baseURLComponent = URLComponents(string: "http://plasticfishes.herokuapp.com/")!
    
    typealias dataHandler = (Data?) -> Void
    
    func get(path: String, succesHandler: dataHandler?)
    {
        get(path: path,  body: nil )
    }
    
    func delete (path: String, body:Data?, succesHandler: dataHandler)
    {
        request("DELETE", path: path
    }
    
/*    class func get(path: String, completionHandler: dataHandler?){
        var requestURLComponent = baseURLComponent
        requestURLComponent.path = path
        var request = URLRequest(url: requestURLComponent.url!)
        request.httpMethod = "GET"
        debugPrint(request)
*/
    
    
    let task = URLSession.shared.dataTask(with: URLRequest)
        {
            (data, response, error) in
            if error != nil
            {
                return
            }
            
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode ==  200, let handler = sucesHandler
            {
                DispatchQueue.main.async {
                    handler(data)
                }
            }
            
            
            guard error == nil
                else
            {
                    return
            }
            print("==============")
            debugPrint(error)
            print("=================")
        }
        
    }
}
