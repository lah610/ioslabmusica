//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


//let sumClouser = {(numbers: [Int]) -> Int
    
//}

let printClouser = {() -> Void in
    print("no resibe nada y no regresa nada")
}

let printClouser2 = {(cadena : String) -> Void in
    print(cadena)
}

let printClouser3 = {() -> Int in
    return 3
}

var letras = ["a","b","x","d","y","f"]


letras.sorted()

letras.sorted{(letra1, letra2) -> Bool in
    return letra1 > letra2
}


letras.sorted{
    return $0 > $1
}


var nombres = ["2Pac","Biggie","Nas","DrDre","Busta","Marshal"]

let nombreCompleto = nombres.map{(nombre) -> String in return nombre + " Swift"}

let nombreCompleto2 = nombres.map{ $0 + " Swift"}

let numbers = [5,7,8,22,77,11]

let numbersLessTen = numbers.filter{(number) -> Bool in
    return number < 10
}

let numbersLessTen2 = numbers.filter{$0 < 10}

let total = numbers.reduce(0){(currentTotal, newValue) -> Int in
    return currentTotal + newValue
}

let total2 = numbers.reduce(0, {$0 + $1})
