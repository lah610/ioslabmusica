//: Playground - noun: a place where people can play

import UIKit
import PlaygroundSupport
//visualJson
//let url = URL(string: "https://www.apple.com")!
let BaseURL = URL(string: "https://www.itunes.apple.com/search?item='mecano'")!

extension URL {
    func withQueries(_ queries:[String : String]) -> URL?{
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        
        components?.queryItems = queries.flatMap{URLQueryItem(name: $0.0, value: $0.1)}
        
        return components?.url
    }
}


let query : [String : String] = [
    "term" : "mecano",
    "limit" : "10"
]

let url = BaseURL.withQueries(query)!

let task = URLSession.shared.dataTask(with: url){(data, response, error) in
    if let data = data, let string =  String(data: data, encoding : .utf8) {
        print(string)
    }
}
print("Esta aqui")
task.resume() //La tarea se ejecuta con esta sentencia

PlaygroundPage.current.needsIndefiniteExecution = true
