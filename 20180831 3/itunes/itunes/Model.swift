//
//  Model.swift
//  itunes
//
//  Created by Barragán Edgar on 01/09/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import Foundation

struct Results: Codable{
    var resultCount : Int
    var results: [Tracks]
}

struct Tracks:Codable {
    var artistName : String
    //var collectionName : String
    var trackName : String
}
