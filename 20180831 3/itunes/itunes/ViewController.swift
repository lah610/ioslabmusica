//
//  ViewController.swift
//  itunes
//
//  Created by Barragán Edgar on 01/09/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tablita: UITableView!
    
    var tracks : [Tracks] = []
    
    let BaseURL = URL(string: "https://www.itunes.apple.com/search?")!
    
    let query : [String : String] = [
        "term" : "mecano",
        "limit" : "10"
    ]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celda", for: indexPath)
        cell.textLabel?.text = tracks[indexPath.row].trackName
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            tracks.remove(at: indexPath.row)
            tablita.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        
        let deleteAction = UIContextualAction(style: .normal, title: "Delete"){
            (action, sourceView, completion) in
            let text = "Listening : " + self.tracks[indexPath.row].trackName
            
            let activityController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
            
            self.present(activityController, animated: true)
        }
        
        let shareAction = UIContextualAction(style: .normal, title: "Share"){
            (action, sourceView, completionHandler) in
            let text = "Listening : " + self.tracks[indexPath.row].trackName
            
            let activityController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
            
            self.present(activityController, animated: true)
        }
        
        shareAction.backgroundColor = UIColor.orange
        
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [deleteAction, shareAction])
        
        return swipeConfiguration
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "Siguiente"{
            let indexPath = tablita.indexPathForSelectedRow
            
            let destination =  segue.destination as! SecondViewController
            
            //destination.fromFirstView = track
        }
    }
    
    
    func fetchData(){
        
        let url = BaseURL.withQueries(query)
        
        let jsonDecoder = JSONDecoder()
        
        let task = URLSession.shared.dataTask(with: url!){(data, response, error) in
            if let data = data, let trackList = try? jsonDecoder.decode(Results.self, from : data) {
                var temp:[Tracks] = []
                trackList.results.forEach({(track) in
                    temp.append(track)
                    
                })
                self.tracks = temp
                self.tablita.reloadData()
                
            }else{
                print(error.debugDescription)
            }
        }
        task.resume() //La tarea se ejecuta con esta sentencia
    }

    /*override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }*/


}

extension URL {
    func withQueries(_ queries:[String : String]) -> URL?{
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        
        components?.queryItems = queries.flatMap{URLQueryItem(name: $0.0, value: $0.1)}
        
        return components?.url
    }
}

