//
//  MVC_01Tests.swift
//  MVC_01Tests
//
//  Created by LUIS ARENAS HERNANDEZ on 14/09/18.
//  Copyright © 2018 LUIS ARENAS HERNANDEZ. All rights reserved.
//

import XCTest
//@testable import MVC_01
@testable import MVC_01
struct DataSource
{
//    var fishes: [String]
    static func fishes() -> Bool
    {
        return true
    }
}

class plasticfishes_mvcTest: XCTestCase
{
    func testFishes()
    {
        let fishes = DataSource.fishes()
        {
            XCTAssertNil(fishes)
            XCTAssertEqual(1,fishes.count, "Must have one fish is an array")
        }
    }
}

class MVC_01Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
}
