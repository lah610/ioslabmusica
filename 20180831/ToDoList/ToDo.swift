//
//  ToDo.swift
//  ToDoList
//
//  Created by Barragán Edgar on 31/08/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import UIKit

struct ToDo{
    var title : String
    var isComplete : Bool
    var dueDate :Date
    var note : String?
}
