//: Playground - noun: a place where people can play

import UIKit
import PlaygroundSupport
//visualJson
//let url = URL(string: "https://www.apple.com")!
let BaseURL = URL(string: "https://www.itunes.apple.com/search?item='mecano'")!

extension URL {
    func withQueries(_ queries)
}


let query : [String : String] = [
    "term" : "mecano"
]

let url = BaseURL.withQueries(query)!

let task = URLSession.shared.dataTask(with: url){(data, response, error) in
    if let data = data, let string =  String(data: data, encoding : .utf8) {
        print(string)
    }
}
print("Esta aqui")
task.resume()

PlaygroundPage.current.needsIndefiniteExecution = true
