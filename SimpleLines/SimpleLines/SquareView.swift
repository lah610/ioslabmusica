//
//  SquareView.swift
//  SimpleLines
//
//  Created by Barragán Edgar on 13/10/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import UIKit

class SquareView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        let aPath =  UIBezierPath()
        aPath.lineWidth = 5
        aPath.move(to: CGPoint(x: 10, y: 0))
        aPath.addLine(to: CGPoint(x: frame.width - 10, y: 0))
        aPath.addQuadCurve(to: CGPoint(x: frame.width, y: 10), controlPoint: CGPoint(x: frame.width, y: 10))
        
        UIColor.black.set()
        aPath.stroke()
        
//        let context = UIGraphicsGetCurrentContext()
//        context?.setLineWidth(2)
//        context?.move(to: CGPoint(x: 10, y: 0))
//        context?.addLine(to: CGPoint(x: frame.width - 10, y: 0))
//        context?.addQuadCurve(to: CGPoint(x: frame.width, y: 10), control: CGPoint(x: frame.width, y: 0))
//
//
//
//        context?.setStrokeColor(UIColor.black.cgColor)
//        context?.strokePath()
        
        
    }

}
