//
//  SimpleView.swift
//  SimpleLines
//
//  Created by Barragán Edgar on 13/10/18.
//  Copyright © 2018 Barragán Edgar. All rights reserved.
//

import UIKit

class SimpleView: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(2)
        context?.move(to: CGPoint(x: 50, y: 50))
        context?.addLine(to: CGPoint(x: 150, y: 50))
        context?.addQuadCurve(to: CGPoint(x: 160, y: 60), control: CGPoint(x: 160, y: 50))
        
        context?.addLine(to: CGPoint(x: 160, y: 150))
        context?.addQuadCurve(to: CGPoint(x: 150, y: 160), control: CGPoint(x: 170, y: 150))
        
        context?.setStrokeColor(UIColor.black.cgColor)
        context?.strokePath()
    }
    

}
